import React,{ Component } from 'react';
import { Button, Checkbox } from 'react-native-material-design';
import {
  Image,
  TextInput,
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

class HappyCook extends Component {
  render() {
    return (
    <View style={styles.contain}>
    <View style={styles.style1}>
    <Image
    style={styles.header}
    source={require('./Images/login_header.jpg')}/>
    <Image
    style={styles.header}
    source={require('./Images/hc_logo_white.png')}/>
    </View>
    <Image
    style={styles.bg}
    source={require('./Images/login_bg.jpg')}>
     <View style={styles.container}>
       <Image
         source={require('./Images/signin_fb.png')}
         style={styles.buttons}
       />
       <Text/>

       <Image
         source={require('./Images/signin_google.png')}
         style={styles.buttons}
       />
       <TextInput
         placeholder='Enter Email ID'
         placeholderTextColor= '#d3d3d3'
         underlineColorAndroid= '#d3d3d3'
         
         style={styles.input}
         keyboardType='email-address'
       />  
       <TextInput
         placeholder='Enter Password'
         placeholderTextColor= '#d3d3d3'

         underlineColorAndroid= '#d3d3d3'
         style={styles.input}
         keyboardType='default'
       />
       <View style={styles.subcont}> 
         <Button
           value='LOGIN' 
           raised={true}
           theme='dark'
           overrides= {{backgroundColor: "#09d830", textColor: "#f3f3f3"}}
           style={styles.button1}
         />
         <Button
           value='SIGNUP' 
           raised={true}
           style={styles.button1}
           overrides= {{backgroundColor: "#f3f3f3", textColor: "#09d830" }}
         />
       </View>
       <Checkbox 
       theme= 'light'
         style={styles.check} 
         label="I agree to the terms and conditions" 
         overrides= {{textColor: '#d3d3d3'}}
       />
       <Text/>

       <View style={styles.extra}>
        
        
        </View> 
       </View>
       </Image> 
       </View> 
    );
  }
}

const styles =StyleSheet.create({
  contain: {
  flex: 1,  

  },
  style1:
  {
    position: 'absolute',
  },
  header:
  { position: 'absolute', 
    flex:0.3,
    resizeMode:'stretch',
    width: null,
    height: null,

  },
   bg: {
    
    flex:0.7,
    width:null,
    height: null,
    resizeMode: 'stretch',
    bottom: 0,
  },
  container: {
  flex:1,
  justifyContent: 'flex-end',
  alignItems:'center',
  paddingHorizontal: 30,
  paddingVertical: 30,
  },
  buttons: {
    paddingBottom:5,
    resizeMode:'stretch',
    width: 264,
    height: 40,
    },
  input:{
    width: 264, 
    color: '#d3d3d3',
    },
  subcont: {
    flexDirection: 'row',
    justifyContent: 'center',
     width:500,

    },
    button1: {
      width:500,
      paddingHorizontal: 36,

    },
    check: {
    	
    },

  text1:
  { color: '#f5f5f5',
    top: 20, 
    textAlign: 'left',
    fontSize: 14,
},
  extra: 
  {
    backgroundColor: '#a1a1a1',
  }

});

AppRegistry.registerComponent('HappyCook', () => HappyCook);
