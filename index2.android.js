

import React, { Component } from 'react';
import { Card, Button , Checkbox} from 'react-native-material-design';
import {
  ToolbarAndroid,
  Image,	
  AppRegistry,
  TextInput,
  StyleSheet,
  Text,
  View
} from 'react-native';

class HappyCook extends Component {
  render() {
    return (
    <View style={styles.main}>     
    <View style={styles.container}>
    <Image style={styles.image1}
    source={require('./Images/food.png')}
    >
    <Text style={styles.text3}>
    Malabar Fish Curry
    </Text>
    </Image>
    <View style={styles.info}>
    <Image style={styles.image2}
    source={require('./Images/minus.png')}
    />
    <Text style={styles.text1}>
    SERVES 1
    </Text>
    <Image style={styles.image4}
    source={require('./Images/plus.png')}
    />
    <Text style={styles.text2}>
    ₹164
    </Text>
    <Image style={styles.image3}
    source={require('./Images/addto.png')}
    />
    </View>
    </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderColor:'#8c878e',


  },
  image1: {
    justifyContent: 'flex-end',
    height: 200,
    width: 373,
    resizeMode: 'stretch',
    borderColor: '#8c878e',
    borderWidth: 1,
  },
  info: {
     
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    width:373,
    backgroundColor: '#fefefe',
    borderColor: '#8c878e',
    borderWidth:1,
  },
  image2: {
    left:35,
    height:25,
    width: 25,
    resizeMode: 'stretch',
  },
  image3: {
    left: 165,
    height:35,
    width: 35,
    resizeMode:'stretch',
  },
  image4: {
    left:55,
    height:25,
    width: 25,
    resizeMode: 'stretch',
  },
  text1: {
    left :45,
    fontSize:16,
    fontWeight: '300',
    color: '#000000',
    textAlign:'center',
  },
  text2: {
    left:115,
    fontSize:20,
    color: '#000000',
    fontWeight: '500',
  },
  text3: {
    left: 55,
    bottom:15,
    fontSize: 22,
    fontWeight: '400',
    color: '#f1f1f1',

  },
  main:
  {
    backgroundColor: '#f1f1f1',
    flex:1,
  }
  
});

AppRegistry.registerComponent('HappyCook', () => HappyCook);
