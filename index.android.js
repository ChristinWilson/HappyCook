/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';

import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

import Main from './components/main';
import About from './components/about';
import Locality from './components/locality';
import Localn from './components/localn';
import Localy from './components/localy';
import Login from './components/login';
import Loginer from './components/loginer';
import Resetdet from './components/resetdet';
import Resetemail from './components/resetemail';
import Subscribe from './components/subscribe';
import Subscribey from './components/subscribey';
import Home from './components/home';
import Works from './components/works';





class HappyCook extends Component {
  render() {
  return (
     <Router>
        <Scene key="root">
          <Scene key="main" component={Main} title="Main" initial={true} hideNavBar={true}/>
          <Scene key="login" component={Login} title="Login" onBack={Actions.main} hideNavBar={true} schema="modal"/>
          <Scene key="loginer" component={Loginer} title="Loginer" onBack={Actions.main} hideNavBar={true} schema="modal" type="replace"/>
          <Scene key="locality" component={Locality} title="Locality" hideNavBar={true} type="reset"/>
          <Scene key="home" component={Home} title="Home" hideNavBar={true}/>
          <Scene key="resetdet" component={Resetdet} title="Resetdet"  hideNavBar={true}/>
          <Scene key="resetemail" component={Resetemail} title="Resetemail"  hideNavBar={true}/>
          <Scene key="localn" component={Localn} title="Localn" hideNavBar={true} type="replace"/>
          <Scene key="localy" component={Localy} title="Localy" hideNavBar={true} type="replace"/>
          <Scene key="subscribe" component={Subscribe} title="Subscribe" hideNavBar={true}/>
          <Scene key="about" component={About} title="About" hideNavBar={true}/>
          <Scene key="works" component={Works} title="Works" hideNavBar={true}/>
        </Scene>
      </Router>
  );
}
}



const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#f5fcff',
    },
    });


AppRegistry.registerComponent('HappyCook', () => HappyCook);
