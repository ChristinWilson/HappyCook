

import React, { Component } from 'react';
import { Button , Checkbox} from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  TextInput,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';

class Loginer extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}>
       <Image
       source={require('./Images/hc_logo_white.png')}
       style={styles.subimage}
       />
       </Image>
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.container}>
       <Text>
       {"\n"}
       </Text>
       <Image
         source={require('./Images/signin_fb.png')}
         style={styles.buttons}
       />
       <Text/>

       <Image
         source={require('./Images/signin_google.png')}
         style={styles.buttons}
       />
       <TextInput
         placeholder='Enter Email ID'
         placeholderTextColor= '#d3d3d3'
         underlineColorAndroid= '#d3d3d3'

         style={styles.input}
         keyboardType='email-address'
       />
       <TextInput
         placeholder='Enter Password'
         placeholderTextColor= '#d3d3d3'

         underlineColorAndroid= '#d3d3d3'
         style={styles.input}
         keyboardType='default'
       />
       <Checkbox
       theme= 'light'
         style={styles.check}
         label="I agree to the terms and conditions"
         overrides= {{textColor: '#f5f5f5'}}
       />
       <View style={styles.subcont}>
         <Button
           value='LOGIN'
           raised={true}
           theme='dark'
           onPress={Actions.home}
           overrides= {{backgroundColor: "#09d830", textColor: "#f3f3f3"}}
           style={styles.button1}
         />
         <Button
           value='SIGNUP'
           raised={true}
           style={styles.button1}
           onPress={Actions.home}
           overrides= {{backgroundColor: "#f3f3f3", textColor: "#09d830" }}
         />
       </View>

      <View style={styles.extra}>
        <Text/>
        <Text style= {styles.text2} numberOfLine='1'>
        Incorrect Password! Please try again!
        </Text>
        <Text/>
        <Text style= {styles.text3} numberOfLine='1' onPress={Actions.resetdet}>
        Forgot your password? Click here to reset

        </Text>
        <Text/>
        </View>

        <Text/>


      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sect: {

  	flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subimage: {
    height:175,
    width: 175,
    resizeMode: 'contain',
  },
  	bg: {

    flex:0.7,
    width:null,
    opacity: 50,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'flex-end',
    paddingBottom:0,
    //bottom:50,
    top:0,
  },
   subcont: {
    flexDirection: 'row',
    justifyContent: 'center',
     width:500,

    },
  input:{
    width: 264,
    color: '#d3d3d3',
    },
    buttons: {
    paddingBottom:5,
    resizeMode:'stretch',
    width: 264,
    height: 40,
    },

  button1: {
      width:500,
      paddingHorizontal: 36,

    },
   check:{
     paddingBottom:30,
   },
  text1:
  { color: '#f5f5f5',
    left :10,
    top: 20,
    textAlign: 'left',
    fontSize: 14,
  },
  extra:
  {  marginHorizontal: 0,
    flexWrap: 'nowrap',
    height: 70,
    bottom: 0,
    backgroundColor: '#a1a1a1',
    position: 'absolute',
    left:     0,
    right:0,
    alignSelf: 'flex-end',
    alignItems:'center',
    justifyContent:'center',



  },
  text2:
  { textAlign: 'center',
    color:'#f1f1f1',
    fontWeight: 'bold',
    fontSize: 14,
  },
  text3:
  {
    textAlign: 'center',
    color: '#f1f1f1',
    fontWeight: '200',

  },
  check:
  {
      }

});

export default Loginer
