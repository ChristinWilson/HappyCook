/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { Tab, TabLayout } from 'react-native-android-tablayout';
import React, { Component } from 'react';
import {
  ScrollView,
  ToolbarAndroid,
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class HappyCook extends Component {
  render() {
  return (
    <ScrollView>
    <View style={styles.style4}>
    <ToolbarAndroid
      actions={[{title: 'filters', icon: require('./Images/more.png'), show: 'always'},
      {title: 'cart', icon: require('./Images/cart.png'), show: 'always'}

      ]}
      navIcon={require('./Images/settings1.png')}
      logo={require('./Images/hc_logo_white.png')}
      style={styles.toolbar}
      />
      <View style={styles.styles7}>
      <Image
    source={require('./Images/about.png')}
    style={styles.image9}/>
      <Image
    source={require('./Images/founders.png')}
    style={styles.image7}/>
    <View style={styles.style8}>
    <Text style={styles.text6}>
    {"\n\n"}
    Not so long ago, a bunch of friends put their heads together and marked an observation.
    {"\n\n"}
    </Text>
    <Text/>
    <Text style={styles.text6}>
The daily hustles of city life have toppled our priorities and taken out of our life a little joy – one called cooking. Surrounding the art of cooking is a bunch of inconveniences, which in today’s world are easily replaceable, thanks to the boom of technology and last-mile delivery. Parallely, the exponentially growing culture of ‘take out’ and ‘home delivery’ have set a precedence for ‘quick meal’ over healthy home-cooked meal.
    {"\n\n"}
    </Text>
    <Text/>
    <Text style={styles.text6}>
Happy cook is a humble effort to bring together convenience, joy of cooking, variety and healthy habits, using technology as an enabler. Day in, day out, we are driven by a dream to create amazing cooking experiences at homes.
    {"\n\n"}
    </Text>
    <Text style={styles.text10}>
    Maximum Joy. Minimum Inconvenience!
    {"\n\n"}
    </Text>
    <Image
  source={require('./Images/gline.png')}
  style={styles.image10}/>
    </View>
      </View>
      </View>
      </ScrollView>

  );
}
}



const styles = StyleSheet.create({
    toolbar: {
      width: null,
      height: 68,
      backgroundColor:'#484848',
    },
    text10: {
      fontSize: 19,
      fontWeight: "700",
      color: "#757575",
    },
    image10: {
      height: 20,
      width: null,
      resizeMode: "stretch",
    },
    style10: {
       alignItems: 'center',
    },
    styles4: {
      flex:1,
      backgroundColor:'#ffffff',
    },
    style8: {
      marginHorizontal: 25,

    },
    image9: {
      flex:0.2,
      height: 100,
      width: null,
      resizeMode: 'stretch',
    },
    style9: {
      fontSize: 23,
      color: "#757575",
      fontWeight: "300",
    },

    styles7: {
      backgroundColor:'#ffffff',
    },

    image7: {
    left:25,
    right:25,
    height:500,
    width: 350,
    resizeMode: 'cover',
    justifyContent: 'center',

  },
   main1: {
      paddingHorizontal: 15,
      paddingTop:12,
      backgroundColor: '#f3f3f3',
    },
    info: {
    borderColor:'#8c878e',
    borderWidth:1,
    height: 50,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  view1: {
    flex:0.1,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',

  },
  text6: {
    fontSize: 15,
    color: '#757575',
  },
   view2: {
    flex:0.24,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
    view3: {
    flex:0.41,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
   view4: {
    flex:0.15,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
  image1: {

    height:200,
    width: null,
    resizeMode: 'stretch',
    justifyContent: 'flex-end',

  },
  image2: {

    height: 25,
    width:25,
    resizeMode: 'stretch',
  },
  image3: {
    height: 35,
    width:35,
    resizeMode: 'stretch',
  },
  text1: {
    fontSize: 16,
    fontWeight:'300',
    color: '#000000',
  },
  text2: {
    left:10,
    fontSize: 20,
    fontWeight:'500',
    color: '#000000',
  },
  text3: {
    left: 30,
    bottom:15,
    fontSize: 22,
    fontWeight: '400',
    color: '#f1f1f1',
  }
});

AppRegistry.registerComponent('HappyCook', () => HappyCook);
