/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { Tab, TabLayout } from 'react-native-android-tablayout';
import React, { Component } from 'react';
import {
  ScrollView,
  ToolbarAndroid,
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Works extends Component {
  render() {
  return (
      <ScrollView>
    <View style={styles.style4}>
    <ToolbarAndroid
      actions={[{title: 'filters', icon: require('./Images/more.png'), show: 'always'},
      {title: 'cart', icon: require('./Images/cart.png'), show: 'always'}

      ]}
      navIcon={require('./Images/settings1.png')}
      logo={require('./Images/hc_logo_white.png')}
      style={styles.toolbar}
      />

      <View style={styles.styles7}>
      <Image
    source={require('./Images/works.png')}
    style={styles.image9}/>
    <View style={styles.style10}>
    <Text style={styles.text9}>
    {"\n"}
    </Text>
    <Text style={styles.text10}>
    60%
    </Text>
    <Text style={styles.text9}>
    {"\n"}
    </Text>
    <Text style={styles.text6}>
    of your overall cooking time goes into sourcing, cutting, cleaning and portioning ingredients. And let us agree, no one likes it! Together, let’s put an end to the 60% productive time that’s getting wasted. Together, let’s bring back the joy of cooking. Together, let’s change the way India cooks!
    {"\n\n"}
    </Text>
    <Image
  source={require('./Images/gline.png')}
  style={styles.image10}/>
  <Text>
  {"\n\n"}
  </Text>
  <Image
source={require('./Images/step1.png')}
style={styles.image8}/>
<Text style={styles.text6}>
{"\n\n"}
Awesome chefs at HappyCook, create great tasting recipes, which are easy to cook and sure to make you an instant star at home.
{"\n\n"}
</Text>

<Text>
{"\n\n"}
</Text>
<Image
source={require('./Images/tep2.png')}
style={styles.image8}/>
<Text style={styles.text6}>
{"\n\n"}
You choose what to cook next, from these recipes listed in HappyCook’s website/App. Select how many portions should the dish serve and when would you like it delivered.
{"\n\n"}
</Text>

<Text>
{"\n\n"}
</Text>
<Image
source={require('./Images/sep3.png')}
style={styles.image8}/>
<Text style={styles.text6}>
{"\n\n"}
HappyCook’s fulfilment receives your order and decodes it into multiple individual ingredients. Fresh ingredients are sourced, cut, cleaned and weighed to exact proportions and neatly packed in a box with a smart recipe card, describing the best way to cook.
{"\n\n"}
</Text>

<Text>
{"\n\n"}
</Text>
<Image
source={require('./Images/sep4.png')}
style={styles.image8}/>
<Text style={styles.text6}>
{"\n\n"}
Follow the cooking instructions card sent along with the box, cook up a great meal and tell all your friends about it!
{"\n\n"}
</Text>
<Image
source={require('./Images/gline.png')}
style={styles.image10}/>
<Text>
{"\n"}
</Text>


    </View>
      </View>

      </View>
        </ScrollView>

  );
}
}



const styles = StyleSheet.create({
    toolbar: {
      width: null,
      height: 68,
      backgroundColor:'#484848',
    },
    text10: {
      fontSize: 80,
      fontWeight: "700",
      color: "#757575",
    },
    image10: {
      height: 20,
      width: 350,
      resizeMode: "stretch",
    },
    style10: {
      marginHorizontal:25,
       alignItems: 'center',
    },
    styles4: {
      flex:1,
      backgroundColor:'#ffffff',
    },
    style8: {
      marginHorizontal: 25,

    },
    text9: {
      fontSize:40,
    },
    image9: {

      height: 100,
      width: null,
      resizeMode: 'stretch',
    },
    image8: {

      height: 160,
      width: 420,
      resizeMode: 'stretch',
    },
    style9: {
      fontSize: 23,
      color: "#757575",
      fontWeight: "300",
    },

    styles7: {

      backgroundColor:'#ffffff',
    },

    image7: {
    left:25,
    right:25,
    height:500,
    width: 350,
    resizeMode: 'cover',
    justifyContent: 'center',

  },
   main1: {
      paddingHorizontal: 15,
      paddingTop:12,
      backgroundColor: '#f3f3f3',
    },
    info: {
    borderColor:'#8c878e',
    borderWidth:1,
    height: 50,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  view1: {
    flex:0.1,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',

  },
  text6: {
    fontSize: 15,
    color: '#757575',
  },
   view2: {
    flex:0.24,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
    view3: {
    flex:0.41,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
   view4: {
    flex:0.15,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
  image1: {

    height:200,
    width: null,
    resizeMode: 'stretch',
    justifyContent: 'flex-end',

  },
  image2: {

    height: 25,
    width:25,
    resizeMode: 'stretch',
  },
  image3: {
    height: 35,
    width:35,
    resizeMode: 'stretch',
  },
  text1: {
    fontSize: 16,
    fontWeight:'300',
    color: '#000000',
  },
  text2: {
    left:10,
    fontSize: 20,
    fontWeight:'500',
    color: '#000000',
  },
  text3: {
    left: 30,
    bottom:15,
    fontSize: 22,
    fontWeight: '400',
    color: '#f1f1f1',
  }
});

export default Works
