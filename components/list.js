/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  ToolbarAndroid,
  ScrollView,
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class List extends Component {
  render() {
  return (

           <ScrollView showsVerticalScrollIndicator={true}>
      <View style={{flex:0.8}}>
      <View style={styles.main1}>
    <Image style={styles.image1}
    source={require('./Images/food.png')}
    >
    <Text style={styles.text3}>
    Malabar Fish Curry
    </Text>
    </Image>
    <View style={styles.info}>
     <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/minus.png')}
    />
    </View>
    <View style={styles.view2}>
     <Text style={styles.text1}>
       SERVES 1
    </Text>
    </View>
    <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/plus.png')}
    />
    </View>
     <View style={styles.view3}>
     <Text style={styles.text2}>
      ₹164
    </Text>
    </View>
    <View style={styles.view4}>
     <Image style={styles.image3}
    source={require('./Images/addto.png')}
    />
    </View>
    </View>
    </View>
    <View style={styles.main1}>
    <Image style={styles.image1}
    source={require('./Images/food.png')}
    >
    <Text style={styles.text3}>
    Malabar Fish Curry
    </Text>
    </Image>
    <View style={styles.info}>
     <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/minus.png')}
    />
    </View>
    <View style={styles.view2}>
     <Text style={styles.text1}>
       SERVES 1
    </Text>
    </View>
    <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/plus.png')}
    />
    </View>
     <View style={styles.view3}>
     <Text style={styles.text2}>
      ₹164
    </Text>
    </View>
    <View style={styles.view4}>
     <Image style={styles.image3}
    source={require('./Images/addto.png')}
    />
    </View>
    </View>
    </View>
    <View style={styles.main1}>
    <Image style={styles.image1}
    source={require('./Images/food.png')}
    >
    <Text style={styles.text3}>
    Malabar Fish Curry
    </Text>
    </Image>
    <View style={styles.info}>
     <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/minus.png')}
    />
    </View>
    <View style={styles.view2}>
     <Text style={styles.text1}>
       SERVES 1
    </Text>
    </View>
    <View style={styles.view1}>
     <Image style={styles.image2}
    source={require('./Images/plus.png')}
    />
    </View>
     <View style={styles.view3}>
     <Text style={styles.text2}>
      ₹164
    </Text>
    </View>
    <View style={styles.view4}>
     <Image style={styles.image3}
    source={require('./Images/addto.png')}
    />
    </View>
    </View>

</View>
</View>
</ScrollView>
);
}
}

const styles = StyleSheet.create({
    toolbar: {
      width: null,
      height: 68,
      backgroundColor:'#484848',
    },

    image4: {

    height:48,
    width: null,
    resizeMode: 'stretch',
    justifyContent: 'center',

  },
   main1: {
      paddingHorizontal: 15,
      paddingTop:12,
      backgroundColor: '#f3f3f3',
    },
    info: {
    borderColor:'#8c878e',
    borderWidth:1,
    height: 50,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  view1: {
    flex:0.1,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',

  },
   view2: {
    flex:0.24,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
    view3: {
    flex:0.41,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
   view4: {
    flex:0.15,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
  image1: {

    height:200,
    width: null,
    resizeMode: 'stretch',
    justifyContent: 'flex-end',

  },
  image2: {

    height: 25,
    width:25,
    resizeMode: 'stretch',
  },
  image3: {
    height: 35,
    width:35,
    resizeMode: 'stretch',
  },
  text1: {
    fontSize: 16,
    fontWeight:'300',
    color: '#000000',
  },
  text2: {
    left:10,
    fontSize: 20,
    fontWeight:'500',
    color: '#000000',
  },
  text3: {
    left: 30,
    bottom:15,
    fontSize: 22,
    fontWeight: '400',
    color: '#f1f1f1',
  }
});

export default List
