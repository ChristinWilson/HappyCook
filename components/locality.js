/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button } from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Locality extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}
       />
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.container}>
        <View style={styles.buttons}>
        <Button value="LOGIN/SIGNUP" raised={true}  theme='dark' overrides={{backgroundColor: "#09d830", textColor: "#f1f1f1"}} onPress={Actions.login}   />
        <Button value="ENTER AS GUEST" overrides={{textColor: "#09d830"}} onPress={Actions.home} />
        </View>
        <View style={styles.style3}>
        <TextInput
        placeholder='Enter Area/Locality'
        placeholderTextColor= '#d3d3d3'
        underlineColorAndroid= '#d3d3d3'
        keyboardType='default'
        style={styles.style2}      />
        <View >
        <Button value="CHECK" raised={true} overrides= {{backgroundColor: "#f1f1f1", textColor: "#09d830"}} onPress={Actions.localy}/>
        </View>
        </View>

      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sect: {

    flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
  },
    bg: {

    flex:0.7,
    width:null,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom:10,
  },
  buttons:{
   //alignSelf: 'center',
    flexDirection: 'row',


  },
  style3: {
  flexDirection: 'row',

  },
  style2: {
    width: 250,

  },



  text1:
  { color: '#f5f5f5',
    top: 7,

    left: 15 ,
    textAlign: 'left',
    fontSize: 14,
  },
  text2:
  {color: '#ffffff',
    textAlign: 'auto',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
});

export default Locality
