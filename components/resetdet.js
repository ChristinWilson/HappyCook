

import React, { Component } from 'react';
import { Button , Checkbox} from 'react-native-material-design';
import {
  Image,	
  AppRegistry,
  TextInput,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';

class Resetdet extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}>
       <Image
       source={require('./Images/hc_logo_white.png')}
       style={styles.subimage}
       />
       </Image>
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.style2}>
       <Text style={styles.text1}>
       RESET PASSWORD
       </Text>
       <TextInput
       placeholder='Enter Email ID'
       placeholderTextColor= '#d3d3d3'
       underlineColorAndroid= '#d3d3d3'
       keyboardType=' email-address'
       selectionColor= '#f1f1f1'
       style={styles.style3}
       />
       <Button value="RESET PASSWORD" raised={true}  theme='dark' overrides={{backgroundColor: "#09d830", textColor: "#f1f1f1"}} style={styles.style4} onPress={Actions.resetemail} />
      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sect: {

  	flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subimage: {
    height:175,
    width: 175,
    resizeMode: 'contain',
  },
  	bg: {
    
    flex:0.7,
    width:null,
    opacity: 50,
    height: null,
    resizeMode: 'stretch',
    
  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'flex-end',
    paddingBottom:30,
    //bottom:50,
    top:0,
  },
   subcont: {
    flexDirection: 'row',
    justifyContent: 'center',
     width:500,

    },
  input:{
    width: 264, 
    color: '#d3d3d3',
    },
    buttons: {
    paddingBottom:5,
    resizeMode:'stretch',
    width: 264,
    height: 40,
    },

  button1: {
      width:500,
      paddingHorizontal: 36,

    },

  text1:
  { color: '#f5f5f5',
    
    textAlign: 'left',
    fontSize: 17,
  },
  extra: 
  {  marginHorizontal: 0,
    flexWrap: 'nowrap',
    height: 80,
    bottom: 0,
    backgroundColor: '#a1a1a1',
    position: 'absolute',
    left:     0,
    right:0,
    alignSelf: 'flex-end',
    alignItems:'center',
    justifyContent:'center',


   
  },
  style2:
  { width: 400,
    alignItems: 'center',

    top: 80,
  },
  style3:
  {width: 200,
  },
  style4:
  {width:150,
      }
 
});

export default Resetdet