

import React, { Component } from 'react';
import { Button , Checkbox} from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  TextInput,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';



class Resetemail extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}>
       <Image
       source={require('./Images/hc_logo_white.png')}
       style={styles.subimage}
       />
       </Image>
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.style2}>
       <Text style={styles.text1} onPress={Actions.resetemail}>
       RESET PASSWORD
       </Text>
       <Text style={styles.text2}  numberOfLine='5'>
       {"\n"} We’ve just sent you an email with a link to reset your password. Once you’ve set a new password, please go back to the home page and login again.
       {"\n"}
       </Text>
        <TouchableHighlight  onPress={Actions.login}>
        <View style={styles.style1} >
        <Image style={styles.style5}
        source={require('./Images/arrow.png')}

        />
        <Text style={styles.style6}
       numberOfLine='1' >
           {"\n"}
           Back to Login
        </Text>
      </View>
      </TouchableHighlight>

      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  style5: {
    height:50,
    width:50,
    resizeMode: 'stretch',
  },
  sect: {

  	flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subimage: {
    height:175,
    width: 175,
    resizeMode: 'contain',
  },
  	bg: {

    flex:0.7,
    width:null,
    opacity: 50,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    //justifyContent: 'flex-end',
    paddingBottom:30,
    //bottom:50,
    top:0,
  },
   subcont: {
    flexDirection: 'row',
    justifyContent: 'center',
     width:500,

    },
  input:{
    width: 264,
    color: '#d3d3d3',
    },
    buttons: {
    paddingBottom:5,
    resizeMode:'stretch',
    width: 264,
    height: 40,
    },

  button1: {
      width:500,
      paddingHorizontal: 36,

    },

  text1:
  { color: '#f5f5f5',
    fontWeight: 'bold',
    textAlign: 'left',
    fontSize: 17,
  },
  extra:
  {  marginHorizontal: 0,
    flexWrap: 'nowrap',
    height: 80,
    bottom: 0,
    backgroundColor: '#a1a1a1',
    position: 'absolute',
    left:     0,
    right:0,
    alignSelf: 'flex-end',
    alignItems:'center',
    justifyContent:'center',



  },
   style6: {
  color: '#09d830',

    textAlign: 'auto',
    fontSize: 14,
  },

  style2:
  { width: 400,
    left: 80,

    top: 80,
  },
  text2:
  {width: 250,
    color: '#f1f1f1',
    fontSize:13,
  },
  style4:
  {width:150,
      },
      style1: {
    flexDirection: 'row',

  },

});

export default Resetemail
