/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button } from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Main extends Component {
  render() {
    return (

     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}
       />
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.container}>
        <View style={styles.buttons}>
        <Button value="LOGIN/SIGNUP" raised={true} onPress={Actions.login} theme='dark' overrides={{backgroundColor: "#09d830", textColor: "#f1f1f1"}}   />
        <Button value="ENTER AS GUEST" onPress={Actions.home} overrides={{textColor: "#09d830"}} />
        </View>
        <Text style= {styles.text1} numberOfLine='3' onPress={Actions.locality} >
           We’re almost getting to a point where we serve all of
            Bengaluru. Click here to check if we deliver in your area.
        </Text>

      </View>

      </Image>

      <View style={styles.logo}>
       <Image
       source={require('./Images/logo.png')}
       style={styles.lg}/>
       </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({

  conta: {
    flex:1,
    position: 'absolute',
  },
  sect: {

  	flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
  },
  	bg: {

    flex:0.7,
    width:null,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom:35,
  },
  buttons:{
   //alignSelf: 'center',
    flexDirection: 'row',


  },


  text1:
  { color: '#f5f5f5',
    top: 7,

    left: 15 ,
    textAlign: 'left',
    fontSize: 14,
  },
  text2:
  {color: '#ffffff',
    textAlign: 'auto',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  logo:
  { flex:0.6,
    position:'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top:0,
  },
  lg:
  {
    height:null,
    width:null,
    resizeMode:'cover',

  },
});

export default Main
