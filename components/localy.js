/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button } from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Localy extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}
       />
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.container}>
        <View style={styles.buttons}>
        <Button value="LOGIN/SIGNUP" raised={true}        onPress={Actions.login} theme='dark' overrides={{backgroundColor: "#09d830", textColor: "#f1f1f1"}}   />
        <Button value="ENTER AS GUEST" overrides={{textColor: "#09d830"}}       onPress={Actions.home} />
        </View>
        <Text/>
         <View style={styles.style1}>
        <Image style={styles.style4}
        source={require('./Images/tick.png')}

        />
        <Text style={styles.style2}
       numberOfLine='1' >
           We deliver in your area. Happy Cooking to you!

        </Text>



        </View>


      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sect: {

    flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
  },
    bg: {

    flex:0.7,
    width:null,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom:42,
  },
  buttons:{
   //alignSelf: 'center',
    flexDirection: 'row',


  },
  style1: {
    flexDirection: 'row',

  },
  style4: {
    height:22,
    width:22,
    resizeMode: 'stretch',
  },
  style2: {
  color: '#f5f5f5',

    textAlign: 'auto',
    fontSize: 15,
  },
  text2:
  {color: '#ffffff',
    textAlign: 'auto',
    fontSize: 15,
    textDecorationLine: 'underline',
  },
});

export default Localy
