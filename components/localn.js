/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button } from 'react-native-material-design';
import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Localn extends Component {
  render() {
    return (
     <View style={styles.sect}>
     <Image
       source={require('./Images/login_header.jpg')}
       style={styles.header}
       />
        <Image
       source={require('./Images/login_bg.jpg')}
       style={styles.bg}>
       <View style={styles.container}>
        <View style={styles.buttons}>
        <Button value="LOGIN/SIGNUP" raised={true}  theme='dark' overrides={{backgroundColor: "#09d830", textColor: "#f1f1f1"}} onPress={Actions.login}   />
        <Button value="ENTER AS GUEST" overrides={{textColor: "#09d830"}} onPress={Actions.home}/>
        </View>
        <Text/>
         <View style={styles.style1}>
        <Image style={styles.style4}
        source={require('./Images/No.png')}

        />
        <View style={styles.style5}>
        <Text style={styles.style2}
       numberOfLine='3' >
           Uh Oh! We’re not yet delivering in your area.


        </Text>
        <Text style={styles.style2}
        numberOfLine='3'
              onPress={Actions.subscribe}>
        Click here to subscribe to our launch alerts!
        </Text>
        </View>



        </View>


      </View>
      </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sect: {

    flex: 1,
  flexDirection: 'column',
  },
  header: {
    flex:0.3,
    width:null,
    height: null,
    resizeMode: 'stretch',
  },
    bg: {

    flex:0.7,
    width:null,
    height: null,
    resizeMode: 'stretch',

  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom:22,
  },
  buttons:{
   //alignSelf: 'center',
    flexDirection: 'row',


  },
  style1: {
    flexDirection: 'row',

  },
  style4: {
    height:27,
    width:27,
    resizeMode: 'stretch',
  },
  style2: {
  color: '#f5f5f5',
    left: 15,
    textAlign: 'left',
    fontSize: 15,
  },
  text2:
  {color: '#ffffff',
    textAlign: 'auto',
    fontSize: 15,
    textDecorationLine: 'underline',
  },
  style5:
  {
    flexDirection:'column',
  }
});

export default Localn
