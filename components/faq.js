/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { Tab, TabLayout } from 'react-native-android-tablayout';
import React, { Component } from 'react';
import {
  ScrollView,
  ToolbarAndroid,
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';


class Faq extends Component {
  render() {
  return (
      <ScrollView>
    <View style={styles.style4}>
    <ToolbarAndroid
      actions={[{title: 'filters', icon: require('./Images/more.png'), show: 'always'},
      {title: 'cart', icon: require('./Images/cart.png'), show: 'always'}

      ]}
      navIcon={require('./Images/settings1.png')}
      logo={require('./Images/hc_logo_white.png')}
      style={styles.toolbar}
      />

      <View style={styles.styles7}>
      <Image
    source={require('./Images/faqs.png')}
    style={styles.image9}/>
    <View style={styles.style10}>

    <Text style={styles.text9}>
    {"\n"}
    1. What is Happy Cook all about?
    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Happy Cook is all about great cooking experiences at home. You can browse through our menu and decide what you’d like to cook at home, and we’ll create a box with exact pre-portioned ingredients (Vegetables, meat, spices etc) along with recipes and get it delivered to your home/office.
    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
      2. Who has created the recipes listed?
    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Our recipes have been created by trained 5-star chefs & celebrity food bloggers. All of the items in our menu have been tested multiple times before being listed.
      {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
      3. Do I need to buy any ingredient before trying out an item?
{"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    No! All ingredients required to make the item comes in the package.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
4. How fresh are the cut vegetables and meat?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    100% fresh! The vegetables are cut and packed just a few hours before delivery deadline. To retain the freshness during transit, the package is often accompanied by icepacks.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
5. Do you also sell ready to eat items?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    As a matter of fact, we do. All items listed under Add-ons are ready to eat.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
6. Is it compulsory to have an account created?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    No. You can order items logged in as a guest. Having an account gives you the advantage of not having to type down the delivery address again and again. Also, you can view your past orders in the history section.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
7. How many people can each portion of curry/rice serve?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    You have the option of choosing that. Our portions can serve a minimum of 1 person and a maximum of 4 persons.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
8. What are the payment options available for ordering?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    You can pay online as well cash on delivery.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
9. Do you have a cut off time for ordering items?

    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Yes, we do. In our endeavor to serve fresh cut vegetables and meat, and cater to a large number of orders, we are forced to keep the cut off time of 9am for same day delivery.

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
10. What are your delivery time slots?


    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    We have the following delivery slots{"\n\n"}

    i. 11-12:30pm{"\n"}
    ii. 3-4:30pm{"\n"}
    iii. 4:30-6pm{"\n"}
    iv. 6-7:30pm{"\n"}

    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
11. Do you have offline stores where I can pick this product up from?


    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Sorry, we don’t.


    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
12. How long will a Happy Cook box stay fresh?


    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Approximately 4 hours in transit, with the icepack support provided by us. If you want to cook later, just keep the oils out and place the box in the refrigerator. We recommend completion of cooking within 3 days of receiving the box.


    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
13. I’d like to gift my friend a Happy Cook Voucher. How do I do that?


    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    Send us a message in the contact us page and we’ll create a voucher and send it to you in 48 hours time 


    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
14. I am an awesome chef and I’d like to be part of the recipe development team. What do I do?



    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>

    Click here, fill in the details and click the submit button. We’ll reach out to you if we see a good fit!


    {"\n\n"}
    </Text>
    </View>

    <Text style={styles.text9}>
    {"\n"}
15. What are your cancellation policies?



    {"\n"}
    </Text>
    <View style={styles.style11}>
    <Text style={styles.text6}>
    We process ingredients on demand, and it becomes very difficult for us to cancel orders. Therefore, we do not permit cancellation currently.



    {"\n\n"}
    </Text>
    </View>

    <Image
  source={require('./components/Images/gline.png')}
  style={styles.image10}/>



    </View>
      </View>

      </View>
        </ScrollView>

  );
}
}



const styles = StyleSheet.create({
    toolbar: {
      width: null,
      height: 68,
      backgroundColor:'#484848',
    },
    styles11: {
      marginLeft:10,
    },
    text10: {
      fontSize: 80,
      fontWeight: "700",
      color: "#757575",
    },
    image10: {
      height: 20,
      width: 350,
      resizeMode: "stretch",
    },
    style10: {
      marginHorizontal:25,

    },
    styles4: {
      flex:1,
      backgroundColor:'#ffffff',
    },
    style8: {
      marginHorizontal: 25,

    },
    text9: {
      fontSize:18,
      fontWeight: 'bold',
    },
    image9: {

      height: 130,
      width: null,
      resizeMode: 'stretch',
    },
    image8: {

      height: 160,
      width: 420,
      resizeMode: 'stretch',
    },
    style9: {
      fontSize: 23,
      color: "#757575",
      fontWeight: "300",
    },

    styles7: {

      backgroundColor:'#ffffff',
    },

    image7: {
    left:25,
    right:25,
    height:500,
    width: 350,
    resizeMode: 'cover',
    justifyContent: 'center',

  },
   main1: {
      paddingHorizontal: 15,
      paddingTop:12,
      backgroundColor: '#f3f3f3',
    },
    info: {
    borderColor:'#8c878e',
    borderWidth:1,
    height: 50,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  view1: {
    flex:0.1,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',

  },
  text6: {
    fontSize: 15,
    color: '#757575',
  },
   view2: {
    flex:0.24,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
    view3: {
    flex:0.41,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
   view4: {
    flex:0.15,
    height: null,
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
   },
  image1: {

    height:200,
    width: null,
    resizeMode: 'stretch',
    justifyContent: 'flex-end',

  },
  image2: {

    height: 25,
    width:25,
    resizeMode: 'stretch',
  },
  image3: {
    height: 35,
    width:35,
    resizeMode: 'stretch',
  },
  text1: {
    fontSize: 16,
    fontWeight:'300',
    color: '#000000',
  },
  text2: {
    left:10,
    fontSize: 20,
    fontWeight:'500',
    color: '#000000',
  },
  text3: {
    left: 30,
    bottom:15,
    fontSize: 22,
    fontWeight: '400',
    color: '#f1f1f1',
  }
});

export default Faq
