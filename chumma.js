/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

import Main from './components/main';
import Locality from './components/locality';
import Localn from './components/localn';
import LocalY from './components/localy';
import Login from './components/login';
import Loginer from './components/loginer';
import Reset from './components/reset';
import Resetemail from './components/resetemail';
import Subscribe from './components/subscribe';
import Subscribey from './components/subscribey';
import Home from './components/home';





class HappyCook extends Component {
  
  renderScene(route, navigator) {
    if(route.name == 'main') {
      return <Main navigator={navigator} />
    }
      if(route.name == 'locality') {
      return <Locality navigator={navigator} />
    }
      if(route.name == 'localn') {
      return <Localn navigator={navigator} />
    }
      if(route.name == 'localy') {
      return <LocalY navigator={navigator} />
    }
      if(route.name == 'login') {
      return <Login navigator={navigator} />
    }
      if(route.name == 'loginer') {
      return <Loginer navigator={navigator} />
    }
      if(route.name == 'reset') {
      return <Reset navigator={navigator} />
    }
      if(route.name == 'resetemail') {
      return <Resetemail navigator={navigator} />
    }
      if(route.name == 'subscribe') {
      return <Subscribe navigator={navigator} />
    }
      if(route.name == 'subscribey') {
      return <Subscribey navigator={navigator} />
    }
  }

  render() {
  return (
    <View style={styles.container}>
    <Navigator
    initialRoute={{name: 'main'}}
    renderScene={this.renderScene.bind(this)}
    />
    </View>
  );
}
}



const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#f5fcff',
    },
    });
  

AppRegistry.registerComponent('HappyCook', () => HappyCook);
